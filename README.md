# Numeric Tolerances (.NET)

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/numeric-tolerances`.*

---

An example pipeline showing how to post-process a delta file to allow for tolerances when comparing numerical data.

This document describes how to run the sample. For concept details see: [Numeric Tolerances](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/numeric-tolerances)

## Running the Sample
The sample can be run via a *run.bat* batch file, so long as this is issued from the sample directory. Alternatively, as this batch file contains a single command, the command can be executed directly:

	..\..\bin\deltaxml.exe compare tolerance file1.xml file2.xml result.xml
	
# **Note - .NET support has been deprecated as of version 10.0.0 **